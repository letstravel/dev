const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const morgan = require('morgan');
const fct = require('./function');
const pay = require('./paypal');
var cors = require('cors')
const expressValidator = require('express-validator');
var session = require('express-session')
var request = require('request');
const db = require('./bdd');


app.use(cors({origin:"*", creadentials: true}))
app.use(bodyParser.urlencoded({ extended: true })); // Accept URL Encoded params
app.use(expressValidator());
app.use(morgan('dev'));
app.use(session({ resave: true ,secret: '123456' , saveUninitialized: true}));

app.use(bodyParser.json()); // Accept JSON Params

app.set('view engine', 'ejs');



// Route login

app.get('/login', (req, res) => {    // modify /login to /signin
    res.render('login', {errorLogin:'', errorRegister:''});
})
app.post('/loginMobile', (req, res) => {    // modify /login to /signin
    return fct.loginMobile(req, res);
})
// Route register

app.post('/register', (req,res) => {
    fct.register(req, res);
})

// Route SignIn

app.post('/signIn', (req,res) => {
    fct.login(req, res);
})

app.get('/logout', (req,res) => {
    fct.logout(req, res);
})

app.post('/contact', (req,res) => {
   fct.contact(req, res);
})

app.get('/aboutUs', (req,res) => {
    fct.aboutUs(req, res);   
 })

app.post('/updateUser', (req,res) => {
   // à refaire (Confirmation email, gestion Token)
})

app.get('/success', (req, res) => {
    pay.successFct(req, res);
});

app.use(express.static("views/public"));

app.get('/', (req, res) => {
    fct.home(req, res);
})

app.post('/search', (req, res) => {
     fct.getDataApi(req, res);
});

app.get('/search/roundTrip', (req, res) => {
	fct.getDataApiRT(req, res);
});

app.get('/optionsTicket', (req,res) => {
    fct.verifyToken(req, res);
    fct.optionsTicket(req, res);  
})

app.get('/passengerData', (req,res) => {
    fct.verifyToken(req, res);
    fct.passengerData(req, res);  
})

app.get('/admin/chart', (req,res) => {
   // fct.verifyToken(req, res);
    fct.chart(req, res);  
})


const data = require('./data.json')
app.get('/chartdata', (req,res) => {
	
	res.json(data);
 })
 

app.post('/passengerData/passenger', (req,res) => {
    fct.verifyToken(req, res);
    fct.postDataPassenger(req, res);  
})

app.post('/payment', (req,res) => {
    fct.verifyToken(req, res);
    fct.payment(req, res);
})

app.get('/paymentConfirmation', (req,res) => {
    fct.verifyToken(req, res);
    fct.paymentConfirmation(req, res);
})

app.get('/paypal', (req,res) => {
    fct.verifyToken(req, res);
    pay.paypalFct(req,res);    // we have to add if(logout) condition
})

app.get('/success', (req, res) => {
    pay.successFct(req, res);
});

app.get('/ticketMobile', (req, res) => {
    fct.ticketMoblie(req,res);   
});

app.post('/dataTick', (req, res) => {
	  
	fct.ticketDB(req, res);
});

app.get('/profile', (req, res) => {
    fct.verifyToken(req, res);
    fct.profile(req, res);
});

app.post('/updateProfile', (req, res) => {
    fct.verifyToken(req, res);
    fct.update(req, res);
});

app.listen(3000, function() {
    console.log('Le serveur est run sur: http://localhost:3000');
});

app.post('/forgot', (req, res) => {
    fct.forgot(req, res);
});

app.get('/meteoMobile', (req, res) => {

    // db.connection.query(`select city distination from ticket where email=?`, [email], function(err, data) {
    //     if (!err){

    //         var ville = 

    //     }
    //     else{
    //         console.error('dataBase error: ', err.message);
    //     }
    // })

    // request({
	// 	method: 'GET',
	// 	uri: `https://api.openweathermap.org/data/2.5/weather?q=${ville},fr&appid=c21a75b667d6f7abb81f118dcf8d4611&units=metric`,

	//   },
	//   function (error, response, body) {
	// 	if (error) {
	// 	  return console.error('upload failed:', error);
	// 	}
	// 	var temp =  JSON.parse(body);

	// 	console.log(temp);
	// 	res.json({temp: temp});
	//   })



// Pour tester l'api 

	request({
		method: 'GET',
		uri: 'https://api.openweathermap.org/data/2.5/weather?q=Paris,fr&appid=c21a75b667d6f7abb81f118dcf8d4611&units=metric',

	  },
	  function (error, response, body) {
		if (error) {
		  return console.error('upload failed:', error);
		}
		var temp =  JSON.parse(body);

		console.log(temp);
		res.json({temp: temp});
	  })

});