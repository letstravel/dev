var request = require('request');


function jsonData(departure, destination, departureDate, child, adult){

  return  {
    "cabinClass":"ECONOMY",
    "discountCode":"",
    "passengerCount":{
      "YOUNG_ADULT":0,
      "INFANT":0,
      "CHILD":child,
      "ADULT":adult
    },
    "currency":"EUR",
    "minimumAccuracy":"",
    "requestedConnections":[
      {
        "origin":{
          "airport":{
            "code":departure
          }
        },
        "destination":{
          "airport":{
            "code":destination
          }
        },
        "departureDate":departureDate
      }
    ],
    "shortest":true
  };
}

function sendRequest(myjson){





  return new Promise((resolve, reject) => {
    

    request({
        url: "https://api.airfranceklm.com/opendata/flightoffers/available-offers",
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "AFKL-TRAVEL-Country": "FR",
            "Accept": "application/hal+json;profile=com.afklm.b2c.flightoffers.available-offers.v1;charset=utf8",
            "AFKL-TRAVEL-Host": "AF",
            "Accept-Language": "en-US",
            "Api-Key": "527snz5eas6r9suhztr63kj3",
            "X-Originating-IP": "94.126.119.20"
        },
        
        json: true,   // <--Very important!!!
        body: myjson
    }, function(err, res, body) {
      if (err) {
        reject(err);
        return;
      }
      resolve(body);
    })
})
}


async function getData(json){
  return await sendRequest(json);
}

exports.getData = getData;
exports.jsonData = jsonData;