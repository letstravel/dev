var nodemailer = require('nodemailer');
var config = require('./config')

var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'letstravel015@gmail.com',
    pass: config.password
  }
});

function mailOptionsRegister(emailUser, nameUser){
  return {
    from: 'letstravel015@gmail.com',
    to: emailUser,
    subject: 'WELCOME !!',
    html: `<h1>Dear ${nameUser}</h1><p>Thank you for registration, your new <strong> Let's Travel </strong> account has been created, welcome to the LT community !!</p>`
  }
};

function mailOptionsContact(emailUser,message){
  return {
    from: 'letstravel015@gmail.com',
    to: emailUser,
    subject: 'WELCOME !!',
    html: message
  }
};

function mailOptionsForgot(emailUser,message){
  return {
    from: 'letstravel015@gmail.com',
    to: emailUser,
    subject: 'Your passoword',
    html: message
  }
};

 function sendEmail (mailOptions){
    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        console.log('Error Email : ', error);
      } else {
        console.log('Email sent: ' + info.response);
      }
    });
}

exports.mailOptionsRegister = mailOptionsRegister;
exports.mailOptionsContact = mailOptionsContact;
exports.sendEmail = sendEmail;
exports.mailOptionsForgot = mailOptionsForgot;

