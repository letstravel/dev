const bcrypt = require('bcrypt');
const db = require('./bdd');
const jwt = require('jsonwebtoken');
const mail = require('./email');
const ls = require('local-storage');
const dataApi = require('./data');






// Encrypt password

var regexTel = /^(0[1-7])(?:[ _.-]?(\d{2})){4}$/;

var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;

function validateLenght(array) {
    var res = true;
    array.forEach(element => {
        if (element.length < 4){
            res = false;
        } 
    });
    return res;
}

var logout = false;
var tokenUser

exports.verifyToken = (req, res) => {
jwt.verify(ls.get('token'), '123456', function(err, decoded) {       
    if (err) {
        logout = false;
        req.session.redirectTo = req.originalUrl;
        ls.remove('token');
        res.redirect('/login');
} else 
{
    // if everything is good, save to request for use in other routes
    logout = true;
    tokenUser = {
        token : ls.get('token'),
        email : jwt.verify(ls.get('token'), '123456').email,
        expAt : jwt.verify(ls.get('token'), '123456').iat
    }

    var refreshToken = jwt.sign({email: tokenUser.email}, '123456', {
        expiresIn: 3600 // expires in 1 hour
    });
    ls.remove('token');
    ls.set('token', refreshToken);
    req.decoded = decoded;         
  }
});
}

exports.register = (req, res) => {
    var errorRegister = '';
    var errorLogin = '';
    var error = '';

    var firstName = req.body.firstName;
    var lastName = req.body.lastName;
    var password = req.body.password;
    var confirmPassword = req.body.confirmPassword;
    var email = req.body.email;
    var confirmEmail = req.body.confirmEmail;
    var gender = req.body.gender;
   
    if (!firstName.length || !lastName.length || !password.length || !confirmPassword.length || !email.length ||
        !confirmEmail.length || !gender.length){
            errorRegister = 'Please Fill All Required Field';
            res.render('login', {errorLogin, errorRegister, error})
            return;
        }

    if (password !== confirmPassword) {
        errorRegister = 'Error confirm password';
        res.render('login', {errorLogin, errorRegister, error})
        return;
    }

    if (email !== confirmEmail) {
        errorRegister = 'Error confirm email';
        res.render('login', {errorLogin, errorRegister, error})
        return;
    }

    var array = [firstName, lastName, password];
    if ( ! validateLenght(array) ) {
        errorRegister = 'Minimum caracters length';
        res.render('login', {errorLogin, errorRegister, error})
        return;
    }

    if (!regexEmail.test(email)) {
        errorRegister = 'Error email not valid';
        res.render('login', {errorLogin, errorRegister, error})
        return;
    } 

    var hashPassword = bcrypt.hashSync(password, 10);

    db.connection.query('select COUNT(*) AS cnt from user where email=?', [email], function(err, rows) {
        if (!err){
            if (rows[0].cnt > 0){
                errorRegister = 'email exists deja';
                res.render('login', {errorLogin, errorRegister, error})
                return;
            } 
            else {
                db.connection.query(`insert into user(email, user_password, firstName, lastName, gender) values(?,?,?,?,?)`, [email, hashPassword, firstName, lastName, gender], function(err) {
                    if (!err){
                        mail.sendEmail(mail.mailOptionsRegister(email, firstName));
                        res.render('login', {errorLogin, errorRegister: 'OK', error})

                    }
                    else{
                        console.error('dataBase error: ', err.message);
                    }
                })
            }
        }
        else{
         console.error('dataBase error: ', err.message);
        }
    });
}





exports.login = (req, res) => {
    var errorLogin = '';
    var errorRegister = '';
    var error = '';

    var username = req.body.username;
    var password = req.body.password;

    if (!username.length || !password.length){
            errorLogin = 'Please Fill All Required Field';
            res.render('login', {errorLogin, errorRegister, error})
            return;
        }
    
    // req.checkBody('username', 'username is required !').notEmpty();
    // var errors = req.validationErrors();

    // if(errors) {
    //     res.render('login', {
    //       errors: errors
    //     });
    // }


    if (!regexEmail.test(username)) {
        errorLogin = 'Error mail not valid';
        res.render('login', {errorLogin, errorRegister, error})
        return;
    } 
    
    var array = [username, password];

    if ( ! validateLenght(array) ) {
        errorLogin = 'Error caracters length';
        res.render('login', {errorLogin, errorRegister, error})
        return;
    }

    db.connection.query('select COUNT(*) AS cnt from user where email=?', [username], function(err, rows) {
        if (!err){
            if (rows[0].cnt === 0){
                errorLogin = 'email/password not exist !';
                res.render('login', {errorLogin, errorRegister, error})
                return;
            } 
            else {
                db.connection.query(`select user_password AS pwd from user where email=?`, [username], function(err, data) {
                    if (!err){
                       if (bcrypt.compareSync(password, data[0].pwd)){
      
                            var token = jwt.sign({email: username}, '123456', {
                                expiresIn: 3600 // expires in 1 hour
                            });
                            console.log('Token : ', token);
                            logout = true;
                            ls.set('token', token);
                            var redirectTo = '/';
                            if (req.session.redirectTo){
                                var redirectTo = req.session.redirectTo;
                                delete req.session.redirectTo;
                            }
                            
                            res.redirect(redirectTo);
                        }
                        else{
                            errorLogin = 'email/password not exist !';
                            res.render('login', {errorLogin, errorRegister, error})
                            return;
                        }
                    }
                    else{
                        console.error('dataBase error: ', err.message);
                    }
                })
            }
        }
        else{
         console.error('dataBase error: ', err.message);
        }
    });
}


exports.loginMobile = (req, res) => {
    var errorLogin = '';
    var error = true;

    var username = req.body.username;
    var password = req.body.password;

    if (!username.length || !password.length){
        res.json({errorLogin: 'Please Fill All Required Field', error})
            return;
        }

    db.connection.query('select COUNT(*) AS cnt from user where email=?', [username], function(err, rows) {
        if (!err){
            if (rows[0].cnt === 0){
                res.json({errorLogin: 'email/password not exist !', error})
                return;
            } 
            else {
                db.connection.query(`select user_password AS pwd from user where email=?`, [username], function(err, data) {
                    if (!err){
                        if (bcrypt.compareSync(password, data[0].pwd)){
      
                            var token = jwt.sign({email: username}, '123456', {
                                expiresIn: 3600 // expires in 1 hour
                            });
                            console.log('TokenMobile : ', token);
                            ls.set('tokenMobile', token);
                            return res.json({errorLogin, error:false})
                        }
                        else{
                            return res.json({errorLogin: 'email/password not exist !', error})
                           
                        }
                    }
                    else{
                        console.error('dataBase error: ', err.message);
                    }
                })
            }
        }
        else{
         console.error('dataBase error: ', err.message);
        }
    });
}

exports.contact = (req, res) => {


    var name = req.body.name;
    var email = req.body.email;
    var phone = req.body.phone;
    var message = req.body.message;

    if (!name.length || !phone.length || !message.length){
        res.json({status: 400, message : "Please fill all required field"});
        return;
    }

    var array = [name, email, message];
    if ( ! validateLenght(array) ) {
        res.json({status: 400, message : "Error: Minimum characters length allowed"});
        return;
    }

    if (!regexEmail.test(email)) {
        res.json({status: 400, message : "Email not valid"});
        return;
    } 

    if (!regexTel.test(phone)){   
        res.json({status: 400, message : "Error: the number phone is not valid"});
        return;
    }
    
    var message = `<h1>Dear ${name}</h1><p>Your message has been sent successfully, Thank you for contact us, we will try to reply as soon as possible.</p> <p> LT community </p>`;
    
    mail.sendEmail(mail.mailOptionsContact(email, message));
    res.json({status: 200, message : "Your message has been sent successfully, Thank you for contact us, we will try to reply as soon as possible "});

}

exports.logout = (req, res) => {
    logout = false;
    ls.remove('token');
    res.redirect('/');
}
exports.home = (req, res) => {
    res.render('index', {logout: logout, error:'', errorLogin:'', errorRegister:''}) 
}
// exports.profile = (req, res) => {
//     res.render('profile', {logout: logout, error:'', errorLogin:'', errorRegister:''}) 
// }
exports.passengerData = (req, res) => {
    if (logout){
        res.render('passengerData', {logout: logout, nbrPassenger: ls.get('nbrPassenger')});
    }
}
exports.postDataPassenger = (req, res) => {

    var error = '';
    var errorLogin = '';
    var errorRegister = '';

    var firstName = req.body.firstName;
    var lastName = req.body.lastName;
    var email = req.body.email;
    var confirmEmail = req.body.confirmEmail;
    var nationality = req.body.nationality;
    var birthday = req.body.birthday;
    var phone = req.body.phone;
    var passport = req.body.passport;

    if (!firstName.length || !lastName.length || !email.length || !confirmEmail.length || !nationality.length || !birthday.length || !passport.length){
        error = 'Please Fill All Required Field !';
         res.render('passengerData', {logout: logout, nbrPassenger: ls.get('nbrPassenger'), error, errorLogin, errorRegister});
        return;
    }

    var array = [firstName, lastName, email, confirmEmail, nationality, birthday, phone, passport];
    if ( ! validateLenght(array) ) {
        error = 'Error: caracters length!';
        res.render('passengerData', {logout: logout, nbrPassenger: ls.get('nbrPassenger'), error, errorLogin, errorRegister});
        return;
     }

    if (!regexEmail.test(email)) {
        error = 'Error: email not valid !';
        res.render('passengerData', {logout: logout, nbrPassenger: ls.get('nbrPassenger'), error, errorLogin, errorRegister});
        return;
    } 

    if (!regexTel.test(phone)){
        error = 'Error: the number phone is not valid !';
        res.render('passengerData', {logout: logout, nbrPassenger: ls.get('nbrPassenger'), error, errorLogin, errorRegister});
        return;
    }
}

exports.optionsTicket = (req, res) => {
    if (logout){
        res.render('optionsTicket', {logout: logout});
    }
}
exports.aboutUs = (req, res) => {
    res.render('aboutUs', {logout: logout});
}
exports.payment = (req, res) => {
    if (logout){
        res.render('payment');
    }
}
exports.paymentConfirmation = (req, res) => {
    if (logout){
        res.render('paymentConfirmation');
    }
}
exports.getDataApiRT = (req, res) => {
    res.render('bookingDataRT', {logout: logout});
}

exports.ticketMoblie = (req, res) => {
    
    db.connection.query(`select * from ticket `, function(err, data) {
        if (!err){
               
            var ticketData = data;

               res.json({ticketData: ticketData});
        }
        else{
            console.error('dataBase error: ', err.message);
        }
    })
}

exports.ticketDB = (req, res) => {
    var data = req.body.data;


  var depart = data[0];
  var  distination  = data[2];
  var  prix =  data[8];
  var  dateDepart = data[6];
  var  dateArrivee =  data[7];

    db.connection.query(`insert into ticket(depart, distination, prix, dateDepart, dateArrivee) values(?,?,?,?,?)`, [depart, distination, prix, dateDepart, dateArrivee], function(err) {
        if (!err){
          console.log('OK DB INSERT')

        }
        else{
            console.error('dataBase error: ', err.message);
        }
    })
}

exports.getDataApi = (req, res) => {

    var departure = req.body.departure;
    var destination = req.body.destination;
    var date = req.body.date;
    var adult = parseInt(req.body.adult);
    var child = parseInt(req.body.child);

    var totalPassenger = adult + child;
    ls.set('nbrPassenger', totalPassenger);


    if (totalPassenger>7){
        error = 'the number of passengers can not exceed 7';
        res.render('index', {errorLogin:'', errorRegister:'', error, logout})
        return;
    }
    
    res.render('bookingData', {logout: logout})
 
}

exports.chart = (req, res) => {
    res.render('chart', {logout: logout}) 
}

exports.profile = (req, res) => {
    var tokenEmail = tokenUser.email;
    db.connection.query('select * from user where email = ?;', [tokenEmail], function(err, rows) {
        if (!err) {
            var data = rows[0];
            res.render('profile', { logout: logout, data: data, error: '', errorLogin: '', errorRegister: '' })
            return data;
        } else {
            console.log('not ok')
        }
    })

}

exports.update = (req, res) => {
     var email = req.body.email;

     db.connection.query('select COUNT(*) AS cnt from user where email=?', [email], function(err, rows) {
        if (!err){
            if (rows[0].cnt > 0){
                error = 'email exists deja';
              res.send(error);
                return;
            } 
            else {
                db.connection.query('UPDATE user SET email = ? WHERE email = ?', [email, tokenUser.email], function(err) {
                    if (!err){
                      console.log('OK')

                    }
                    else{
                        console.error('dataBase error: ', err.message);
                    }
                })
            }
        }
        else{
         console.error('dataBase error: ', err.message);
        }
    });
}

exports.forgot = (req, res) => {

    var email = req.body.emailForgot;

    db.connection.query('select COUNT(*) AS cnt from user where email=?', [email], function(err, rows) {
        if (!err){
            if (rows[0].cnt === 0){
                errorLogin = 'email not exist !';
                res.render('login', {errorLogin, errorRegister: ' ', error: ''})
                return;
            } 

        }
    })
    
    db.connection.query(`select user_password AS pwd from user where email=?`, [emailForgot], function(err, data) {
        if (!err){

            var message = '<p> Password : ' +  data[0].pwd + ' </p>'
            mail.sendEmail(mail.mailOptionsForgot(email, message));
        }
        else{
            console.error('dataBase error: ', err.message);
        }
    })

}