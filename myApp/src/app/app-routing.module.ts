import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'historique', loadChildren: './pages/historique/historique.module#HistoriquePageModule' },
  { path: 'preparation', loadChildren: './pages/preparation/preparation.module#PreparationPageModule' },
  { path: 'accueil', loadChildren: './pages/accueil/accueil.module#AccueilPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
