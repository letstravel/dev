import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.page.html',
  styleUrls: ['./accueil.page.scss'],
})
export class AccueilPage implements OnInit {

  constructor(private route: Router) { }

  ngOnInit() {
  }
  openHistorique(){
    this.route.navigate(['historique']);
  }


  openVoyage(){
      this.route.navigate(['preparation'])
  }
}
