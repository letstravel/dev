import { Component, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-historique',
  templateUrl: './historique.page.html',
  styleUrls: ['./historique.page.scss'],
})
export class HistoriquePage implements OnInit {
  ticketList;
 
  constructor(public actionSheetController: ActionSheetController, private route: Router, private ticket: UserService) {}

  ngOnInit() {
  }

  openBillet(){
  
    this.ticket.historique().subscribe(
      (ticket: any)=>{
        if (ticket.ticketData)
      {

          this.ticketList = ticket.ticketData;

      } else {
        
      }
      }
    ),(err)=>{console.log(err)}
    
  }



}
