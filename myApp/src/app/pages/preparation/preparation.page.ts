import { Component, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-preparation',
  templateUrl: './preparation.page.html',
  styleUrls: ['./preparation.page.scss'],
})
export class PreparationPage implements OnInit {
  meteoData;
 
  constructor(public actionSheetController: ActionSheetController, private route: Router, private prepar: UserService) {}



  ngOnInit() {
  }

  prepa(){
  
    this.prepar.preparation().subscribe(
      (prepar: any)=>{
        if (prepar.temp)
      {
        console.log(prepar.temp);
          this.meteoData = prepar;
    
      } else {
        
      }
      }
    ),(err)=>{console.log(err)}
    
  }

}
