import { Injectable } from '@angular/core';
import { Config } from 'src/config/config';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private config : Config, private http : HttpClient) { }

  /* Returns an Observable for a GET request.
   * @param endpointUrl The endpoint url relative to the base API url.
   * @param options Request options.
   */
  public get(endpointUrl : string) {
    const url = this.getUrl(endpointUrl);
    return this.http.get(url);
  }

  /* Returns an Observable for a POST request.
   * @param endpointUrl The endpoint url relative to the base API url.
   * @param body The body of the request.
   * @param options Request options.
   */
  public post(endpointUrl: string, body ?: any) {
    const url = this.getUrl(endpointUrl);
    return this.http.post(url, body);
  }

  /* Returns an Observable for a PUT request.
   * @param endpointUrl The endpoint url relative to the base API url.
   * @param body The body of the request.
   * @param options Request options.
   */
  public put(endpointUrl: string, body?: any) {
    const url = this.getUrl(endpointUrl);
    return this.http.put(url, body);
  }

  /* Returns an Observable for a DELETE request.
   * @param endpointUrl The endpoint url relative to the base API url.
   * @param options Request options.
   */
  public delete(endpointUrl : string) {
    const url = this.getUrl(endpointUrl);
    return this.http.delete(url);
  }

  getUrl(relativeUrl : string) {
    return new URL(relativeUrl, this.config.API_BASE_URL).toString();
  }
}
