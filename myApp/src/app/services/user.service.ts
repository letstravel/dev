import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
export class User{
  username: string;
  password: string;
}
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private api: ApiService) { }
  login(user: User){
     return this.api.post('/loginMobile',user);
  }

  historique(){
    return this.api.get('/ticketMobile');
  }

  preparation(){
    return this.api.get('/meteoMobile');
  }

}