import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class Config {
  public readonly API_BASE_URL = "http://localhost:3000/"
}